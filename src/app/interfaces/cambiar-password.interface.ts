export interface CambiarPasswordInterface {

    old_pass: string,
    new_pass: string,
    id_login: number

}