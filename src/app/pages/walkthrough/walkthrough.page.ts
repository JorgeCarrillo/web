import { LoginPage } from './../login/login.page';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { NavController, IonSlides, MenuController, ActionSheetController, ModalController, ToastController, LoadingController } from '@ionic/angular';
import { environment } from "../../../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { AuthService } from "../../services/auth.service";
import { SearchFilterPage } from '../../pages/modal/search-filter/search-filter.page';

import {
    trigger,
    style,
    animate,
    transition,
    query,

    stagger
} from '@angular/animations';

@Component({
    selector: 'app-walkthrough',
    templateUrl: './walkthrough.page.html',
    styleUrls: ['./walkthrough.page.scss'],
    animations: [
        trigger('staggerIn', [
            transition('* => *', [
                query(':enter', style({ opacity: 0, transform: `translate3d(0,10px,0)` }), { optional: true }),
                query(':enter', stagger('300ms', [animate('600ms', style({ opacity: 1, transform: `translate3d(0,0,0)` }))]), { optional: true })
            ])
        ])
    ]
})

export class WalkthroughPage implements OnInit {
    @ViewChild(IonSlides, { static: true }) slides: IonSlides;
    showSkip = true;
    slideOpts = {
        effect: 'flip',
        speed: 1000
    };
    dir: String = 'ltr';

    slideList: Array<any> = [
        {
            title: 'Que es <strong><span class="text-tertiary">AMAUTA</span> </strong>?',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque maximus, dui accumsan cursus lacinia, nisl risus.',
            image: 'assets/img/house01.png',
        },
        {
            title: 'Por que es mejor <strong><span class="text-tertiary">AMAUTA</span> </strong>?',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque maximus, dui accumsan cursus lacinia, nisl risus.',
            image: 'assets/img/business01.png',
        },
        {
            title: '<strong>Estas en el lugar perfecto!</strong>',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque maximus, dui accumsan cursus lacinia, nisl risus.',
            image: 'assets/img/rent01.png',
        }
    ];
    auxcategorias: any;
    tipoUsuario: string;
    constructor(
        public navCtrl: NavController,
        public menuCtrl: MenuController,
        public router: Router,
        public _httpClient: HttpClient,
        public _authService: AuthService,
        public modalCtrl: ModalController,
    ) {
        this.AllCategorias()
        this.menuCtrl.enable(true);
        if (this._authService.getRol() === null) {
            this.tipoUsuario = 'CLIENTE'
        } else {
            this.tipoUsuario = this._authService.getRol()
        }
    }


    ionViewWillEnter() {
        this.AllCategorias()
    }

    ngOnInit() {
        this.AllCategorias()
    }

    onSlideNext() {
        this.slides.slideNext(1000, false);
    }

    onSlidePrev() {
        this.slides.slidePrev(300);
    }

    // onLastSlide() {
    // 	this.slides.slideTo(3, 300)
    // }

    openHomeLocation() {
        this.navCtrl.navigateRoot('/home-results');
        // this.router.navigateByUrl('/tabs/(home:home)');
    }
    goInicio() {
        this.navCtrl.navigateRoot('/#intro');
    }
    openLoginPage() {
        this.navCtrl.navigateForward('/login');
    }

    openRegisterPage() {
        this.navCtrl.navigateForward('/register');
    }

    async AllCategorias() {

        {
            this._httpClient.get(environment.url + 'categoria',
                {
                    headers: new HttpHeaders({
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + this._authService.getToken()
                    })
                })
                .subscribe(datacategorias => {
                    this.auxcategorias = datacategorias;
                }, error => {
                    console.log(error)
                });

        }
    }

    seleCategoria(nombrecategoria) {
        alert("hola" + nombrecategoria)
        if (nombrecategoria != null || nombrecategoria != undefined) {
            let navigationExtras: NavigationExtras = {
                state: {
                    selectcategoria: nombrecategoria
                }
            };

            this.router.navigate(['home-results'], navigationExtras);
        };
    }


}
