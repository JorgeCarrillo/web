import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, MenuController, ToastController, AlertController, LoadingController } from '@ionic/angular';
import { TranslateProvider } from '../../providers';
import { LoginInterface } from "../../interfaces/login.interface";
import { AuthInterface } from '../../interfaces/auth.interface';
import { UserInterface } from "../../interfaces/user.interface";
import { environment } from "../../../environments/environment";
import { AuthService } from "../../services/auth.service";
import { Router } from "@angular/router";
import { UsuarioService } from "../../services/usuario.service";
import * as CryptoJS from 'crypto-js';
import { JwtHelperService } from '@auth0/angular-jwt';
import { EnvioCorreoServiceService } from "../../services/envio-correo-service.service";
import { AppComponent } from '../../app.component';
import { sha256 } from 'js-sha256';


@Component({
  selector: 'app-recover-password',
  templateUrl: './recover-password.page.html',
  styleUrls: ['./recover-password.page.scss'],
})
export class RecoverPasswordPage implements OnInit {
  contrasenia;
  repitecontrasenia;
  codigo;
  constructor(
    public _authservice: AuthService,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private translate: TranslateProvider,
    private formBuilder: FormBuilder,
    private authSvc: AuthService,
    private router: Router,
    private _jwtHelper: JwtHelperService,
    private readonly _usuarioService: UsuarioService,
    private _correo: EnvioCorreoServiceService,
    private appcomponet: AppComponent,
  ) {

  }

  ngOnInit() {
  }
  async cambiarContrasenia() {
    if (this.repitecontrasenia == this.contrasenia) {
      let body = {
        codigo: sha256(this.codigo),
        contraseña: sha256(this.contrasenia),
        correo: this._authservice.usuarioRecuperar
      }
      const loader = await this.loadingCtrl.create({
        duration: 2000
      });
      loader.present();
      this._authservice.actualizarContrasenia(body).subscribe(res => {
        if (res) {
          loader.present();
          loader.onWillDismiss().then(async l => {
            const toast = await this.toastCtrl.create({
              showCloseButton: true,
              message: "Contraseña actualizada correctamente",
              duration: 3000,
              position: 'bottom'
            });

            toast.present();
            this.navCtrl.pop()
          });
        } else {
          loader.present();
          loader.onWillDismiss().then(async l => {
            const toast = await this.toastCtrl.create({
              showCloseButton: true,
              message: "Codigo incorrecto vuelva a intentarlo",
              duration: 3000,
              position: 'bottom'
            });

            toast.present();

          });




        }
      })
    }
  }
}
