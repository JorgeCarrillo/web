import { NgModule, LOCALE_ID } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { AgmCoreModule } from '@agm/core';

import { IonicModule } from '@ionic/angular';

import { PropertyDetailPage } from './property-detail.page';
import { NgCalendarModule } from "ionic2-calendar";
import { StarRatingModule } from "ionic4-star-rating";

const routes: Routes = [
  {
    path: '',
    component: PropertyDetailPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild(),
    NgCalendarModule,
    StarRatingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD9BxeSvt3u--Oj-_GD-qG2nPr1uODrR0Y'
    })
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'es-EC' }
  ],
  declarations: [PropertyDetailPage]
})
export class PropertyDetailPageModule { }
