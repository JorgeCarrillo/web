import { Component, ViewChild, OnInit } from '@angular/core';
import { IonItemSliding } from '@ionic/angular';
import { MessageService } from '../../providers/message/message.service';
import { AuthService } from '../../services/auth.service'
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
@Component({
  selector: 'app-messages',
  templateUrl: './messages.page.html',
  styleUrls: ['./messages.page.scss'],
})
export class MessagesPage implements OnInit {
  messages: Array<any> = [];
  idrol;
  @ViewChild('slidingList', { static: false }) slidingList: IonItemSliding;

  constructor(public messageService: MessageService, public _AuthService: AuthService, public router: Router) { }

  ngOnInit() {

  }
  ionViewDidEnter() {
    this.getMessages();
  }

  async deleteItem(message) {
    this.messageService.delMessage(message);
    await this.slidingList.close().then(() => { });
  }
  async brokerChat(broker) {
    let navigationExtras: NavigationExtras = {
      state: {
        broker: broker
      }
    };
    this.router.navigate(['broker-chat'], navigationExtras);
  }
  getMessages() {
    this.idrol = this._AuthService.getIdUsuarioRol()
    this.messages = this.messageService.getMessages(this.idrol);
  }
}
