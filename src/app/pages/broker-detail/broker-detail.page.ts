import { Component } from '@angular/core';
import { NavController, ActionSheetController, ModalController, ToastController } from '@ionic/angular';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

import { ImagePage } from './../modal/image/image.page';

import {
  BrokerService, PropertyService,
} from '../../providers';

import {
  trigger,
  style,
  animate,
  transition,
  query,
  stagger
} from '@angular/animations';

@Component({
  selector: 'app-broker-detail',
  templateUrl: './broker-detail.page.html',
  styleUrls: ['./broker-detail.page.scss'],
  animations: [
    trigger('staggerIn', [
      transition('* => *', [
        query(':enter', style({ opacity: 0, transform: `translate3d(0,10px,0)` }), { optional: true }),
        query(':enter', stagger('300ms', [animate('600ms', style({ opacity: 1, transform: `translate3d(0,0,0)` }))]), { optional: true })
      ])
    ])
  ]
})
export class BrokerDetailPage {
  brokerID: any;
  broker: any;
  items: any = [];
  itemHeight: number = 0;
  brokers: any[];
  datos = {
    img: "",
    telefono: "",
    correo: "",
    nombre: ""
  }
  propierties;
  constructor(
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    public modalCtrl: ModalController,
    public route: ActivatedRoute,
    public router: Router,
    private brokerService: BrokerService,
    private service: PropertyService
  ) {
    this.brokerID = this.route.snapshot.paramMap.get('id');
    this.broker = this.brokerService.getItem(this.brokerID) ?
      this.brokerService.getItem(this.brokerID) :
      this.brokerService.getbrokers()[0];
  }

  ionViewWillEnter() {
    this.propierties = this.service.properties
    for (let a of this.propierties) {
      if (a.id == this.brokerID) {
        this.datos.telefono = a.phone
        this.datos.img = a.picture
        this.datos.correo = a.email
        this.datos.nombre = a.title
      }
    }
  }

  async scheduleVisit(broker) {
    let navigationExtras: NavigationExtras = {

      state: {
        broker: broker,
      }
    };

    this.router.navigate(['property-detail/' + broker], navigationExtras);
  };

  async presentImage(image: any) {
    const modal = await this.modalCtrl.create({
      component: ImagePage,
      componentProps: { value: image }
    });
    return await modal.present();
  }

  async brokerChat(broker) {
    let navigationExtras: NavigationExtras = {
      state: {
        broker: broker
      }
    };

    this.router.navigate(['broker-chat'], navigationExtras);
  }

  expandItem(item) {

    this.items.map((listItem) => {

      if (item == listItem) {
        listItem.expanded = !listItem.expanded;
      } else {
        listItem.expanded = false;
      }

      return listItem;

    });

  }

}
