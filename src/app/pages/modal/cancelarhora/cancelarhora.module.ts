import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { NgCalendarModule } from "ionic2-calendar";
import { CancelarhoraPage } from './cancelarhora.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgCalendarModule
    
  ],
  declarations: [CancelarhoraPage]
})
export class CancelarhoraPageModule {}
