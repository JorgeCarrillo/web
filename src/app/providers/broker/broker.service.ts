import { Injectable } from '@angular/core';
//import brokers from './mock-brokers';
import { environment } from "../../../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { AuthService } from "../../services/auth.service";
import { EnvioCorreoServiceService } from "../../services/envio-correo-service.service";
import { Router } from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class BrokerService {


  constructor(private readonly _httpClient: HttpClient,
    private readonly _authService: AuthService,
    private readonly _correo: EnvioCorreoServiceService,
    private router: Router) {
    this.getAllTeachers()
  }


  favoriteCounter = 0;
  favorites: Array<any> = [];
  brokers: Array<any>;
  async getAllTeachers() {
    this.brokers = []
    let aux: any
    this._httpClient.get(environment.url + 'usuario/profesores',
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this._authService.getToken()
        })
      }
    )
      .subscribe(data => {

        aux = data
        for (let i = 0; i < aux.length; i++) {
          let foto
          if (aux[i].foto_usuario === null) {
            foto = 'avatar.png'
          } else {
            foto = aux[i].foto_usuario
          }
          this.brokers.push({


            id: aux[i].id_usuario,
            address: aux[i].direccion_usuario,
            city: aux[i].ciudad_nombre,
            state: "MA",
            zip: aux[i].id_usuario,
            price: "",
            title: aux[i].nombres_usuario + " " + aux[i].apellidos_usuario,
            estrellas: aux[i].estrellas.promedio,
            bedrooms: aux[i],
            bathrooms: 3,
            long: -71.11095,
            lat: 42.35663,
            //picture: environment.url + 'public/users/img/profesores/' + aux[i].foto_usuario,
            picture: foto,
            thumbnail: environment.url + 'public/users/' + aux[i].foto_usuario,
            ensena: "",
            images: [
              "assets/img/properties/house01.jpg",
              "assets/img/properties/house03.jpg",
              "assets/img/properties/house06.jpg",
              "assets/img/properties/house08.jpg"
            ],
            tags: "suburban",
            description: "",
            label: aux[i].estado_usuario,
            facebook: "",
            instagram: "",
            twitter: "",
            youtube: "",
            square: 152,
            broker: {
              id: 1,
              name: aux[i].nombres_usuario + " " + aux[i].apellidos_usuario,
              title: "Matemáticas, Idiomas",
              picture: environment.url + 'public/users/' + aux[i].foto_usuario,
            },
            phone: aux[i].telefono_usuario,
            mobilePhone: aux[i].telefono_usuario,
            email: aux[i].correo_usuario,


          })
        }
      }, error => {
      });
  }

  findAll() {
    this.getAllTeachers()
    return Promise.resolve(this.brokers);
  }
  getbrokers() {
    return this.brokers;
  }

  findById(id) {
    this.getAllTeachers()
    return Promise.resolve(this.brokers[id - 1]);
  }

  getItem(id) {
    this.getAllTeachers()
    for (let i = 0; i < this.brokers.length; i++) {

      if (this.brokers[i].id === parseInt(id)) {
        return this.brokers[i];

      }
    }
    return null;

  }

  getFavorites() {
    return Promise.resolve(this.favorites);
  }

  favorite(broker) {
    this.favoriteCounter = this.favoriteCounter + 1;
    // this.favoriteCounter += 1;
    this.favorites.push({ id: this.favoriteCounter, broker: broker });
    return Promise.resolve();
  }

  unfavorite(favorite) {
    const ind = this.favorites.indexOf(favorite);
    if (ind > -1) {
      this.favorites.splice(ind, 1);
    }
    return Promise.resolve();
  }

}
