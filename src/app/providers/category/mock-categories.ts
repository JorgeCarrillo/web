const categories: Array<any> = [
    {
        id: 1,
        name: 'Matemáticas',
        value: 'suburban',
        picture: 'assets/img/properties/house01.jpg',
        quantity: 2
    },
    {
        id: 2,
        name: 'Idiomas',
        value: 'colonial',
        picture: 'assets/img/properties/house02.jpg',
        quantity: 2
    },
];

export default categories;
