import {UserInterface} from "../interfaces/user.interface";


export class RoleValidator {
    isSuscriptor(user: UserInterface): boolean {
        return user.role === 'ESTUDIANTE';
    }

    isEditor(user: UserInterface): boolean {
        return user.role === 'PROFESOR';
    }

    isAdmin(user: UserInterface): boolean {
        return user.role === 'ADMIN';
    }
}
