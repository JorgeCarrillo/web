import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {map} from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class CategoriasService  {
  labelAttribute = "name";

  constructor(private http:HttpClient) {

  }

}
