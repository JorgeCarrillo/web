import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {CambiarPasswordInterface} from "../interfaces/cambiar-password.interface";
import {environment} from "../../environments/environment";
import {AuthService} from "./auth.service";

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
      private readonly _httpClient: HttpClient,
      private readonly _authSer: AuthService
  ) {
  }

  async cambiarPassword(id, data, token): Promise<any> {

    return await this._httpClient.put(environment.url + 'login/' + id, {
      password_login: data
    }, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    })
        .toPromise()
        .then(result => result)
        .catch(error => alert(error.error.message))

  }

  async changeOldPassword(data: CambiarPasswordInterface): Promise<any> {

    try {
      return await this._httpClient.post(environment.url + 'login/change_password', data, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this._authSer.getToken()
        })
      }).toPromise()
          .then(result => result)
      //.catch(e => alert(e.error ? e.error.mensaje : e.message))

    } catch (e) {
      alert(e.error ? e.error.mensaje : e.message);
    }


  }
}
